# 农产品交易管理平台

#### 项目介绍
- 功能包括：农产品发布，每日价格显示、客户管理、订单管理、线下接单量统计、线上接单量统计、热销产品统计，热销省份统计、交易金额统计、成交量统计，每日出库，入库
记录和交易记录等，交易报表生成，交易数据分析、成本计算和盈利计算等。

#### 软件架构
- 分布式微服务架构

  ![输入图片说明](common_service/src/main/resources/%E9%A1%B9%E7%9B%AE%E6%9E%B6%E6%9E%84%E5%9B%BE.jpg)
- 使用技术

  SpringCloudAlibaba、Nacos、OpenFeign、GateWay、Springboot、MybatisPlus、MySQL、Redis、Vue。
- 图片存储

  七牛云

#### 使用教程
- 导入SQL
- 图片存储可以用七牛云或阿里云对象存储
- 安装 Nacos、Redis
- 克隆后端项目，修改配置文件并启动
- 启动web前端项目
- 前端项目可以build和后端项目混合部署，也可以分离部署


