package com.farmer.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.farmer.common.entity.Buy;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@Mapper
public interface BuyMapper extends BaseMapper<Buy> {
    @Select("select * from tb_buy where user_id = #{userId}")
    public Buy getBuy(Integer userId);
}
