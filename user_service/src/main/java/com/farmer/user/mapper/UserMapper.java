package com.farmer.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.farmer.common.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
