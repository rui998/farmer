package com.farmer.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.farmer.common.entity.Address;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@Mapper
public interface AddressMapper extends BaseMapper<Address> {
    @Select("select * from tb_address where user_id = #{userId}")
    List<Address> addressList(Integer userId);

}
