package com.farmer.user.client;


import com.farmer.common.dto.ShopDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("shop-service")
public interface ShopClient {

    @ApiOperation("根据id查询商品详情")
    @GetMapping("/shop_service/{shopId}")
    ShopDTO getByShopId(@PathVariable Integer shopId);
}
