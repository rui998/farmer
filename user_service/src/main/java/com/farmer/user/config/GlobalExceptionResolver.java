package com.farmer.user.config;


import com.farmer.common.constants.Code;
import com.farmer.common.entity.ResModel;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常处理类
 */
@ControllerAdvice
@Log4j2
public class GlobalExceptionResolver {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResModel exceptionHandler(Exception e){
        System.err.println("出现的异常=>："+e);
        return ResModel.error(Code.FAIL).add("ExceptionMessage",e.getMessage());
    }

}
