package com.farmer.user.service;

import com.farmer.common.entity.Buy;
import com.baomidou.mybatisplus.extension.service.IService;
import com.farmer.common.entity.ResModel;
import io.swagger.models.auth.In;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
public interface BuyService extends IService<Buy> {

    ResModel addShop(Integer shopId,Integer userId);

    Buy creBuyCart(Integer id);

    ResModel findShop(Integer cartId);

    ResModel delShop(Integer shopId, Integer cartId);

}
