package com.farmer.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.farmer.common.entity.Address;
import com.farmer.common.entity.ResModel;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
public interface AddressService extends IService<Address> {

    ResModel updateAddress(Address address);

    ResModel findAddress(Integer addressId);

    ResModel delAddress(Integer addressId);

    ResModel addAddress(Address address);

    Address getAddress(Integer addressId);
}
