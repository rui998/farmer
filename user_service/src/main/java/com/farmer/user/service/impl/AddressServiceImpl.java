package com.farmer.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.farmer.common.constants.Code;
import com.farmer.common.entity.Address;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.farmer.common.entity.ResModel;
import com.farmer.user.mapper.AddressMapper;
import com.farmer.user.service.AddressService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@Service
public class AddressServiceImpl extends ServiceImpl<AddressMapper, Address> implements AddressService {

    @Resource
    private AddressMapper addressMapper;
    //修改地址
    @Override
    public ResModel updateAddress(Address address) {
       if(address.getAddress()==null){
           return ResModel.error(Code.UN_UPDATE);
       }else{
           address.setUpdateTime(LocalDateTime.now());
           updateById(address);
           return ResModel.success(Code.UPDATE,address);
       }
    }

    //查询地址
    @Override
    public ResModel findAddress(Integer userId) {
        LambdaQueryWrapper<Address> w = new LambdaQueryWrapper<>();
        w.eq(true,Address::getUserId,userId);
        List<Address> addresses = list(w);
        return  ResModel.success(Code.SUCCESS,addresses);

    }

    //根据id删除地址
    @Override
    public ResModel delAddress(Integer addressId) {
        return  ResModel.success(Code.DELETE,removeById(addressId));
    }

    //添加地址
    @Override
    public ResModel addAddress(Address address) {
        address.setAddress(address.getAddress());
        save(address);
        return ResModel.success(Code.INSERT,address);
    }

    @Override
    public Address getAddress(Integer addressId) {
        return getById(addressId);
    }
}
