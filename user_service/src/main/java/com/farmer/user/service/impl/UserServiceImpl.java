package com.farmer.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.farmer.common.constants.Code;
import com.farmer.common.entity.Buy;
import com.farmer.common.entity.PageResult;
import com.farmer.common.entity.ResModel;
import com.farmer.common.entity.User;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.farmer.common.utils.MD5Utils;
import com.farmer.common.utils.NowDateUtils;
import com.farmer.user.mapper.UserMapper;
import com.farmer.user.service.BuyService;
import com.farmer.user.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {


    @Resource
    private BuyService buyService;

    /**
     * 登录
     * @param user
     * @return
     */
    @Override
    public ResModel login(User user) {
        //用户名空值
        if(user.getUsername()==null){
            return ResModel.error(Code.CHECK_CODE_ERROR);
        }
        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userLambdaQueryWrapper.eq(true,User::getUsername,user.getUsername());
        User one = getOne(userLambdaQueryWrapper);
        //
        if(one==null){
            //用户不存在 先注册
            return ResModel.error(Code.FAIL);
        }
        String pwd = MD5Utils.encryption(user.getPassword()+one.getSalt());
        if(pwd.equals(one.getPassword())){
            return ResModel.success(Code.LOGIN,one);
        }else{
            return ResModel.error(Code.UN_LOGIN);
        }
    }

    //注册
    @Transactional
    @Override
    public ResModel register(User user) {
        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userLambdaQueryWrapper.eq(true,User::getUsername,user.getUsername());
        User one = getOne(userLambdaQueryWrapper);
        if(one!=null){
            //用户名已存在
            return ResModel.error(Code.REGISTERED);
        }else {
            user.setSalt(NowDateUtils.getSalt());
            user.setUsername(user.getUsername());
            user.setAvatar(user.getAvatar());
            user.setEmail(user.getEmail());
            String pwd = MD5Utils.encryption(user.getPassword()+user.getSalt());
            user.setPassword(pwd);
            save(user);
            Buy buy = buyService.creBuyCart(user.getId());
            return ResModel.success(Code.REGISTRATION,buy);
        }
    }

    //个人信息修改
    @Override
    public ResModel upData(User user){
        updateById(user);
        return ResModel.success(Code.UPDATE,user);
    }

    //根据id删除用户
    @Override
    public ResModel deleteByUserId(Integer userId){
        return  ResModel.success(Code.DELETE,removeById(userId));
    }

    //批量删除用户
    @Override
    public ResModel batchDelete(List<Integer> ids) {
        return ResModel.success(Code.DELETE,removeByIds(ids));
    }

    //分页查询用户
    @Override
    public ResModel listByPage(Integer current, Integer size) {
        Page<User> page = new Page<>(current, size);
        Page<User> selectPage = baseMapper.selectPage(page, null);
        PageResult<User> result = new PageResult<>();
        result.setCurrent(selectPage.getCurrent());
        result.setSize(selectPage.getSize());
        result.setTotal(selectPage.getTotal());
        result.setData(selectPage.getRecords());
        return ResModel.success(Code.SUCCESS,result);
    }

    @Override
    public User getUser(Integer userId) {
        return getById(userId);
    }


}
