package com.farmer.user.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.farmer.common.constants.Code;
import com.farmer.common.dto.ShopDTO;
import com.farmer.common.entity.Buy;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.farmer.common.entity.ResModel;
import com.farmer.common.entity.User;
import com.farmer.user.client.ShopClient;
import com.farmer.user.mapper.BuyMapper;
import com.farmer.user.service.BuyService;
import com.farmer.user.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@Service
public class BuyServiceImpl extends ServiceImpl<BuyMapper, Buy> implements BuyService {

    @Resource
    private BuyMapper buyMapper;
    @Resource
    private ShopClient shopClient;


    //添加商品
    @Override
    public ResModel addShop(Integer shopId,Integer userId) {
        if(shopId==null){
            return ResModel.error(Code.NON_EXISTENT);
        }else {
            Buy buy = buyMapper.getBuy(userId);
            String shopIds = buy.getShopIds();
            if(shopIds==null){
                buy.setShopIds(shopId.toString());
            }else {
                shopIds= shopIds+","+shopId;
                buy.setShopIds(shopIds);
            }
            updateById(buy);
            return ResModel.success(Code.INSERT,buy);
        }
    }

    @Override
    public Buy creBuyCart(Integer id) {
        Buy buy = new Buy();
        buy.setUserId(id);
        save(buy);
        return buy;
    }

    //查询商品
    @Override
    public ResModel findShop(Integer userId) {
        LambdaQueryWrapper<Buy> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(true,Buy::getUserId,userId);
        Buy byId = getOne(wrapper);
        String shopIds = byId.getShopIds();
        if(shopIds==null){
            return ResModel.error(Code.FAIL);
        }else {
            ArrayList<ShopDTO> shopDTOS = new ArrayList<>();
            String[] shops = shopIds.split(",");
            for (int i = 0; i < shops.length; i++) {
                ShopDTO byShopId = shopClient.getByShopId(Integer.parseInt(shops[i]));
                shopDTOS.add(byShopId);
            }
            return ResModel.success(Code.SUCCESS,shopDTOS);
        }
    }

    //购物车内删除商品
    @Override
    public ResModel delShop(Integer shopId,Integer userId) {
        LambdaQueryWrapper<Buy> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(true,Buy::getUserId,userId);
        Buy cart = getOne(wrapper);
        String shopIds = cart.getShopIds();
        if (StrUtil.isNotEmpty(shopIds)){
            String[] shops = shopIds.split(",");
            String tmp="";
            int i=0;
            for (String shop : shops) {
                if (!shop.equals(String.valueOf(shopId))){
                    if (i>0){
                        tmp+=",";
                    }
                    tmp+=shop;
                    i++;
                }
            }
            cart.setShopIds(tmp);
            updateById(cart);
        }

        return ResModel.success(Code.DELETE,shopId);
    }
}
