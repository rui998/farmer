package com.farmer.user.service;

import com.farmer.common.entity.ResModel;
import com.farmer.common.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
public interface UserService extends IService<User> {

    //登录
    ResModel login(User user);
    //注册
    ResModel register(User user);
    //个人信息修改
    ResModel upData(User user);
    //根据用户id删除
    ResModel deleteByUserId(Integer userId);
    //批量删除用户
    ResModel batchDelete(List<Integer> ids);
    //分页查询用户
    ResModel listByPage(Integer current, Integer size);

    User getUser(Integer userId);
}
