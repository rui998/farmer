package com.farmer.user.controller;


import com.farmer.common.entity.ResModel;
import com.farmer.user.service.BuyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@RestController
@RequestMapping("/user_service/buy")
public class BuyController {

    @Resource
    private BuyService buyService;

    @ApiOperation("添加商品")
    @GetMapping(path = "/{shopId}/{userId}",produces = "application/json")
    public ResModel addShop(@PathVariable Integer shopId,@PathVariable Integer userId){
        return buyService.addShop(shopId,userId);
    }

    @ApiOperation("查找商品")
    @GetMapping(path = "/findShop/{userId}",produces = "application/json")
    public ResModel findShop(@PathVariable Integer userId){
        return buyService.findShop(userId);
    }

    @ApiOperation("删除商品")
    @DeleteMapping(path = "/delete/{shopId}/{userId}",produces = "application/json")
    public ResModel delShop(@PathVariable Integer shopId,@PathVariable Integer userId){
        return buyService.delShop(shopId,userId);
    }

}

