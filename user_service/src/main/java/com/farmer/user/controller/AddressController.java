package com.farmer.user.controller;


import com.farmer.common.entity.Address;
import com.farmer.common.entity.ResModel;
import com.farmer.user.service.AddressService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@RestController
@RequestMapping("/user_service/address")
public class AddressController {

    @Resource
    private AddressService addressService;

    @ApiOperation("添加地址")
    @PostMapping(path = "/addAddress",produces = "application/json")
    public ResModel addAddress(@RequestBody Address address){
        return addressService.addAddress(address);
    }

    @ApiOperation("删除地址")
    @DeleteMapping(path="/{addressId}",produces = "application/json")
    public ResModel delAddress(@PathVariable Integer addressId){
        return  addressService.delAddress(addressId);
    }

    @ApiOperation("查询地址")
    @GetMapping(path="/user/{userId}",produces = "application/json")
    public ResModel findAddress(@PathVariable Integer userId){
        return  addressService.findAddress(userId);
    }

    @ApiOperation("查询地址")
    @GetMapping(path="/{addressId}",produces = "application/json")
    public Address getAddress(@PathVariable Integer addressId){
        return  addressService.getAddress(addressId);
    }

    @ApiOperation("修改地址")
    @PostMapping(path = "/updateAddress",produces = "application/json")
    public ResModel updateAddress(@RequestBody Address address){
        return addressService.updateAddress(address);
    }

}

