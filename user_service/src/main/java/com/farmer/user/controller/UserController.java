package com.farmer.user.controller;


import com.farmer.common.entity.ResModel;
import com.farmer.common.entity.User;
import com.farmer.user.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@RestController
@RequestMapping("/user_service")
public class UserController {

    @Resource
    private UserService userService;

    @ApiOperation("登录")
    @PostMapping(path = "/login",produces = "application/json")
    public ResModel login(@RequestBody User user){
        return userService.login(user);
    }

    @ApiOperation("注册")
    @PostMapping(path = "/register",produces = "application/json")
    public ResModel register(@RequestBody User user){
        return  userService.register(user);
    }

    @ApiOperation("信息修改")
    @PostMapping(path = "/amend",produces = "application/json")
    public ResModel amend(@RequestBody User user){
        return userService.upData(user);
    }

    @ApiOperation("根据id删除用户")
    @DeleteMapping(path="/{userId}")
    public ResModel deleteByUserId(@PathVariable Integer userId){
        return  userService.deleteByUserId(userId);
    }

    @ApiOperation("查询用户")
    @GetMapping(path="/{userId}")
    public User getUser(@PathVariable Integer userId){
        return  userService.getUser(userId);
    }

    @ApiOperation("根据id集合批量删除用户")
    @PostMapping(path = "/batch",produces = "application/json")
    public ResModel batchDelete(@RequestBody List<Integer> ids){
        return userService.batchDelete(ids);
    }

    @ApiOperation("分页查询订单详情")
    @GetMapping("/{current}/{size}")
    public ResModel listByPage(@PathVariable Integer current,@PathVariable Integer size){
        return userService.listByPage(current,size);
    }

}

