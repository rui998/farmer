package com.farmer.user;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UserApplicationTests {

    @Test
    void run(){

        GlobalConfig config = new GlobalConfig();
        config.setActiveRecord(true)//�Ƿ�֧��ARģʽ
                .setAuthor("林邵晨")//����
                .setOutputDir("C:\\Users\\86178\\Desktop\\周五答辩\\farmer\\shop_service"+"/src/main/java")//����·��
                .setFileOverride(true)//�ļ�����
                .setIdType(IdType.AUTO)//��������
                .setServiceName("%sService")//��������service�ӿ����ֵ�����ĸ�Ƿ�ΪI��Ĭ�ϻ�����I��ͷ��IStudentService��
                //.setBaseResultMap(true)//�Զ�SQLӳ���ļ������ɻ�����ResultMap
                //.setBaseColumnList(true)//���ɻ�����SQLƬ��
                .setSwagger2(true); //ʵ������ Swagger2 ע��

        //2.����Դ����
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DbType.MYSQL)//�������ݿ�����
                .setDriverName("com.mysql.cj.jdbc.Driver")//���ݿ�������
                .setUrl("jdbc:mysql://localhost:3306/farmer_system_db?useUnicode=true&characterEncoding=UTF-8")//���ݿ��ַ
                .setUsername("root")//���ݿ�����
                .setPassword("root");//���ݿ�����

        //3.��������
        StrategyConfig strategy = new StrategyConfig();
        strategy.setCapitalMode(true)//ȫ�ִ�д����
                .setNaming(NamingStrategy.underline_to_camel)//���ݿ��ӳ�䵽ʵ�����������
                .setColumnNaming(NamingStrategy.underline_to_camel)//�е�����Ҳ֧���շ���������
                //.setTablePrefix("tb")//���ݿ���ǰ׺
                .setInclude("tb_comment")//����Ҫӳ��ı������������д���
                .setEntityLombokModel(true)  //ʹ��Lombok����ע��
                //.setLogicDeleteFieldName("deleted")//�����߼�ɾ���ֶ�
                //.setTableFillList(tableFills)//�����Զ��������
                //.setVersionFieldName("version")//�ֹ�������
                .setRestControllerStyle(true)//�����շ�������ʽ
                .setControllerMappingHyphenStyle(true);//controller�㣬�����»���url : //localhost:8080/hello_id_2

        //4.��������
        PackageConfig packageConfig = new PackageConfig();
        packageConfig
                .setModuleName("shop")//����ģ����
                .setParent("com.farmer")//�����õİ�(����)
                .setMapper("mapper")//Mapper��
                .setService("service")//������
                .setController("controller")//���Ʋ�
                .setEntity("entity");//ʵ����
        //.setXml("mapper");//ӳ���ļ�
        //5.��������
        AutoGenerator autoGenerator = new AutoGenerator();
        autoGenerator.setGlobalConfig(config)
                .setDataSource(dataSourceConfig)
                .setStrategy(strategy)
                .setPackageInfo(packageConfig);
        //6.ִ��
        autoGenerator.execute();
    }

}
