package com.farmer.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.farmer.common.entity.Shop;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@Mapper
public interface ShopMapper extends BaseMapper<Shop> {

    @Select("select * from tb_shop where user_id=#{userId} and deleted=1")
    List<Shop> getDeletedShopList(Integer userId);

}
