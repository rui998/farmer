package com.farmer.shop.mapper;

import com.farmer.common.entity.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 评论表 Mapper 接口
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-09
 */
@Mapper
public interface CommentMapper extends BaseMapper<Comment> {

}
