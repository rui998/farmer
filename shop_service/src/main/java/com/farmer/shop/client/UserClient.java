package com.farmer.shop.client;

import com.farmer.common.entity.User;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("user-service")
public interface UserClient {

    @ApiOperation("查询用户")
    @GetMapping(path="/user_service/{userId}")
    User getUser(@PathVariable Integer userId);

}
