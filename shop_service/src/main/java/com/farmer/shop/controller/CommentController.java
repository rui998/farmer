package com.farmer.shop.controller;


import com.farmer.common.entity.Comment;
import com.farmer.common.entity.ResModel;
import com.farmer.shop.service.CommentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 评论表 前端控制器
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-09
 */
@RestController
@RequestMapping("/shop_service/comment")
public class CommentController {

    @Resource
    private CommentService commentService;

    @ApiOperation("根据商品id查询评论列表")
    @GetMapping("/{shopId}")
    public ResModel getCommentList(@PathVariable Integer shopId){
        return commentService.getCommentList(shopId);
    }

    @ApiOperation("根据用户id查询评论列表")
    @GetMapping("/user/{userId}")
    public ResModel getNewCommentList(@PathVariable Integer userId){
        return commentService.getNewCommentList(userId);
    }

    @ApiOperation("发布评论")
    @PostMapping(path = "/issue",produces = "application/json")
    public ResModel issueComment(@RequestBody Comment comment){
        return commentService.issueComment(comment);
    }

}

