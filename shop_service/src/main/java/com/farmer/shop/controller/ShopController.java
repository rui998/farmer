package com.farmer.shop.controller;


import com.farmer.common.dto.ShopDTO;
import com.farmer.common.entity.ResModel;
import com.farmer.common.entity.Shop;
import com.farmer.common.utils.UploadPicUtils;
import com.farmer.shop.service.ShopService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@RestController
@RequestMapping("/shop_service")
public class ShopController {

    @Resource
    private ShopService shopService;

    @ApiOperation("根据userId查询商品列表")
    @GetMapping("/user/{userId}")
    public List<ShopDTO> getByShopList(@PathVariable Integer userId){
        return shopService.getByShopList(userId);
    }

    @ApiOperation("根据userId查询下架商品")
    @GetMapping("/user/deleted/{userId}")
    public List<ShopDTO> getDeleteShopList(@PathVariable Integer userId){
        return shopService.getDeleteShopList(userId);
    }

    @ApiOperation("根据id查询商品详情")
    @GetMapping("/{shopId}")
    public ShopDTO getByShopId(@PathVariable Integer shopId){
        return shopService.getByShopId(shopId);
    }

    @ApiOperation("扣库存")
    @GetMapping("/stock/{shopId}")
    public boolean reduceStock (@PathVariable Integer shopId){
        return shopService.reduceStock(shopId);
    }

    @ApiOperation("分页查询商品详情")
    @GetMapping("/{current}/{size}")
    public ResModel listByPage(@PathVariable Integer current,@PathVariable Integer size,Shop shop){
        return shopService.listByPage(current,size,shop);
    }

    @ApiOperation("根据id删除商品")
    @DeleteMapping("/{shopId}")
    public ResModel deleteByShopId(@PathVariable Integer shopId){
        return shopService.deleteByShopId(shopId);
    }

    @ApiOperation("根据id集合批量删除")
    @PostMapping(path = "/batch",produces = "application/json")
    public ResModel batchDelete(@RequestBody List<Integer> ids){
        return shopService.batchDelete(ids);
    }

    @ApiOperation("根据id修改商品信息")
    @PutMapping(path = "/update",produces = "application/json")
    public ResModel updateShop(@RequestBody ShopDTO shopDTO){
        return shopService.updateShop(shopDTO);
    }

    @ApiOperation("新增商品")
    @PostMapping(path = "/save",produces = "application/json")
    public ResModel saveShop(@RequestBody ShopDTO shop){
        return shopService.saveShop(shop);
    }

    @ApiOperation("上传图片")
    @PostMapping(path = "/upload",produces = "application/json")
    public String saveShop(MultipartFile file){
        return UploadPicUtils.uploadPicture(file);
    }

}

