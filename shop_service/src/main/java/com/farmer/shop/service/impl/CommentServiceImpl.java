package com.farmer.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.farmer.common.constants.Code;
import com.farmer.common.dto.CommentDTO;
import com.farmer.common.dto.ShopDTO;
import com.farmer.common.entity.Comment;
import com.farmer.common.entity.ResModel;
import com.farmer.common.entity.User;
import com.farmer.common.utils.NowDateUtils;
import com.farmer.shop.client.UserClient;
import com.farmer.shop.mapper.CommentMapper;
import com.farmer.shop.service.CommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.farmer.shop.service.ShopService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>
 * 评论表 服务实现类
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-09
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

    @Resource
    private UserClient userClient;

    @Resource
    private ShopService shopService;

    /**
     * 根据商品id查询评论列表
     * @param shopId
     * @return
     */
    @Override
    public ResModel getCommentList(Integer shopId) {
        LambdaQueryWrapper<Comment> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(true,Comment::getShopId,shopId);
        wrapper.orderByDesc(true,Comment::getCreateTime);
        List<Comment> comments = list(wrapper);
        ArrayList<CommentDTO> dtoArrayList = new ArrayList<>();
        for (Comment comment : comments) {
            CommentDTO commentDTO = BeanUtil.copyProperties(comment, CommentDTO.class);
            commentDTO.setCreateTime(NowDateUtils.format(comment.getCreateTime()));
            User user = userClient.getUser(comment.getUserId());
            commentDTO.setAvatar(user.getAvatar());
            commentDTO.setUsername(user.getUsername());
            dtoArrayList.add(commentDTO);
        }
        return ResModel.success(Code.SUCCESS,dtoArrayList);
    }

    /**
     * 发布评论
     * @param comment
     * @return
     */
    @Override
    public ResModel issueComment(Comment comment) {
        save(comment);
        return ResModel.success(Code.INSERT,comment);
    }

    /**
     * 根据用户id查询评论列表
     * @param userId
     * @return
     */
    @Override
    public ResModel getNewCommentList(Integer userId) {
        List<ShopDTO> shopDTOList = shopService.getByShopList(userId);
        ArrayList<CommentDTO> res = new ArrayList<>();
        if (!CollectionUtils.isEmpty(shopDTOList)){
            for (ShopDTO shopDTO : shopDTOList) {
                Integer id = shopDTO.getId();
                List<CommentDTO> data = (List<CommentDTO>) getCommentList(id).getData();
                for (CommentDTO dto : data) {
                    dto.setShopImg(shopDTO.getShopImg());
                }
                res.addAll(data);
            }
        }
        return ResModel.success(Code.SUCCESS,res);
    }
}
