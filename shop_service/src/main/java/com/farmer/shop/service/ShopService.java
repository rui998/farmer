package com.farmer.shop.service;

import com.farmer.common.dto.ShopDTO;
import com.farmer.common.entity.ResModel;
import com.farmer.common.entity.Shop;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
public interface ShopService extends IService<Shop> {

    ShopDTO getByShopId(Integer shopId);

    ResModel deleteByShopId(Integer shopId);

    ResModel batchDelete(List<Integer> ids);

    ResModel updateShop(ShopDTO shopDTO);

    ResModel saveShop(ShopDTO shop);

    ResModel listByPage(Integer current, Integer size, Shop shopName);

    boolean reduceStock(Integer shopId);

    List<ShopDTO> getByShopList(Integer userId);

    List<ShopDTO> getDeleteShopList(Integer userId);
}
