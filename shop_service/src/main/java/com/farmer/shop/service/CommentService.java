package com.farmer.shop.service;

import com.farmer.common.entity.Comment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.farmer.common.entity.ResModel;

/**
 * <p>
 * 评论表 服务类
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-09
 */
public interface CommentService extends IService<Comment> {

    ResModel getCommentList(Integer shopId);

    ResModel issueComment(Comment comment);

    ResModel getNewCommentList(Integer userId);
}
