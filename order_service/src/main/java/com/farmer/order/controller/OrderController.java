package com.farmer.order.controller;


import com.farmer.common.entity.Order;
import com.farmer.common.entity.ResModel;
import com.farmer.order.service.OrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private OrderService orderService;

    @ApiOperation("根据id查询订单详情")
    @GetMapping("/{orderId}")
    public ResModel getByOrderId(@PathVariable Integer orderId){
        return orderService.getByOrderId(orderId);
    }

    @ApiOperation("分页查询订单详情")
    @GetMapping("/{userId}/{current}/{size}")
    public ResModel listByPage(@PathVariable Integer userId,@PathVariable Integer current,@PathVariable Integer size){
        return orderService.listByPage(userId,current,size);
    }

    @ApiOperation("根据id删除订单")
    @DeleteMapping("/{orderId}")
    public ResModel deleteByOrderId(@PathVariable Integer orderId){
        return orderService.deleteByOrderId(orderId);
    }

    @ApiOperation("根据id集合批量删除")
    @PostMapping(path = "/batch",produces = "application/json")
    public ResModel batchDelete(@RequestBody List<Integer> ids){
        return orderService.batchDelete(ids);
    }

    @ApiOperation("下单")
    @PostMapping(path = "/save",produces = "application/json")
    public ResModel saveOrder(@RequestBody Order order){
        return orderService.saveOrder(order);
    }
}

