package com.farmer.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.farmer.common.entity.Order;
import com.farmer.common.entity.ResModel;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
public interface OrderService extends IService<Order> {

    ResModel getByOrderId(Integer orderId);

    ResModel listByPage(Integer userId, Integer current, Integer size);

    ResModel deleteByOrderId(Integer orderId);

    ResModel batchDelete(List<Integer> ids);

    ResModel saveOrder(Order order);
}
