package com.farmer.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.farmer.common.entity.Order;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

}
