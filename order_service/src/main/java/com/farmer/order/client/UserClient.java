package com.farmer.order.client;

import com.farmer.common.entity.Address;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("user-service")
public interface UserClient {

    @ApiOperation("查询地址")
    @GetMapping(path="/user_service/address/{addressId}",produces = "application/json")
    public Address getAddress(@PathVariable Integer addressId);
}
