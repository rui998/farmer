package com.farmer.common.constants;

/**
 * 状态枚举类
 */
public enum State {

    READ("已读"),
    UNREAD("未读"),

    RECEIVED("已接"),
    UNRECEIVED("未接"),

    NORMAL("正常"),
    BANNED("封禁"),
    LOGOUT("注销"),

    CHECKED("已审核"),
    UNCHECKED("未审核"),

    AUTHORIZED("已认证"),
    UNAUTHORIZED("未认证"),

    USED_IDLE("二手闲置"),
    LOST_PROPERTY("失物招领"),
    SHARE_RESOURCES("资源共享");

    private final String state;

    private State(String state) {
        this.state=state;
    }
    public String getState(){
        return this.state;
    }
}
