package com.farmer.common.constants;

import org.omg.PortableInterceptor.NON_EXISTENT;

/**
 * 响应消息枚举类
 */
public enum Code {

    SUCCESS(200,"成功"),
    FAIL(500,"失败"),

    REGISTERED(500,"已注册"),
    REGISTRATION(200,"注册成功"),
    UNREGISTERED(500,"未注册"),
    LOGIN(200,"登录成功"),
    NOT_LOGGED_IN(500,"未登录"),
    LOGOUT(200,"退出成功"),
    UN_LOGIN(500,"邮箱或密码错误"),
    EMAIL_NULL(500,"邮箱为空"),
    CHECK_CODE_ERROR(500,"邮箱验证码错误"),

    UPDATE(200,"修改成功"),
    UN_UPDATE(500,"修改失败"),
    DELETE(200,"删除成功"),
    UN_DELETE(200,"删除失败"),
    INSERT(200,"添加成功"),
    UN_INSERT(500,"添加失败"),
    AUTHORIZED(200,"认证成功"),
    UNAUTHORIZED(500,"认证失败"),

    ISSUE(200,"发布成功"),
    UN_ISSUE(500,"发布失败"),
    COLLECT(200,"收藏成功"),
    LIKE(200,"点赞成功"),
    COMMENT(200,"评论成功"),
    NON_EXISTENT(500,"不存在");



    /**
     * 响应状态码
     */
    private final int code;
    /**
     * 响应提示
     */
    private final String state;

    private Code(int code, String state) {
        this.code=code;
        this.state=state;
    }
    public String getState(){
        return this.state;
    }

    public int getCode() {
        return this.code;
    }
}
