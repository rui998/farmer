package com.farmer.common.config;


import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常处理类
 */
@ControllerAdvice
@Log4j2
public class GlobalExceptionResolver {


    @ExceptionHandler(Exception.class) // 写Exception.class就是把Exception及其所有子类异常都处理了
    @ResponseBody   // 将方法的返回值以json的格式返回
    public ResponseEntity<String> exceptionHandler(Exception e){
        System.err.println("出现的异常=>："+e);
        // TODO: 2022/9/24 记录日志
        return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
