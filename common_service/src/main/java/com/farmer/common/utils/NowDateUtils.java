package com.farmer.common.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * 获取当前时间工具类
 */
public class NowDateUtils {

    //获取当前时间
    public static Date getNowDate(){
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        return date;
    }

    //获取6位时间戳
    public static String getSalt(){
        long time = new Date().getTime();
        String salt=Long.toString(time).substring(7,13);
        return salt;
    }


    public static String getOrderId(){
        long time = new Date().getTime();
        String salt=Long.toString(time);
        return salt;
    }

    //时间格式化
    public static String format(LocalDateTime time){
        DateTimeFormatter dfDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return dfDateTime.format(time);
    }

}
