package com.farmer.common.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="TbShop对象", description="商品表")
@TableName(value = "tb_shop",autoResultMap = true)
public class Shop extends Model<Shop> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "上传用户的id")
    private Integer userId;

    @ApiModelProperty(value = "商品名称")
    private String shopTitle;

    @ApiModelProperty(value = "商品描述")
    private String shopDesc;

    @ApiModelProperty(value = "商品现价")
    private String shopCurrentPrice;

    @ApiModelProperty(value = "商品原价")
    private BigDecimal shopPrePrice;

    @ApiModelProperty(value = "商品库存")
    private Integer shopStock;

    @ApiModelProperty(value = "商品人气")
    private Integer shopLike;

    @ApiModelProperty(value = "商品评论数量")
    private Integer shopComment;

    @ApiModelProperty(value = "商品收藏数量")
    private Integer shopBuy;

    @ApiModelProperty(value = "商品品牌信息")
    private String shopBrand;

    @ApiModelProperty(value = "商品规格")
    private String shopSpecification;

    @ApiModelProperty(value = "商品图片")
    private String shopImg;

    @ApiModelProperty(value = "商品促销信息")
    private String shopActive;

    @ApiModelProperty(value = "商品信息更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "商品分类")
    private Integer type;

    @ApiModelProperty(value = "逻辑删除 0未删除 1删除")
    private Integer deleted;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
