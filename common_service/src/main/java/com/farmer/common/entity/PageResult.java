package com.farmer.common.entity;

import lombok.Data;

import java.util.List;

@Data
public class PageResult<T> {

    private Long current;
    private Long size;
    private Long total;
    private List<T> data;
}
