package com.farmer.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="TbAddress对象", description="地址表")
@TableName(value = "tb_address",autoResultMap = true)
public class Address extends Model<Address> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "地区")
    private String area;

    @ApiModelProperty(value = "电话")
    private String phone;

    @ApiModelProperty(value = "电话")
    private String username;

    private LocalDateTime updateTime;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "逻辑删除 0未删除 1删除")
    private Integer deleted;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
