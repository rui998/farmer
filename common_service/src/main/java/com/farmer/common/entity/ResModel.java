package com.farmer.common.entity;

import com.farmer.common.constants.Code;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 统一响应实体类
 * @param <T>
 * author 林邵晨
 */
@Api("响应实体类")
@Data
public class ResModel<T> {

    @ApiModelProperty("请求状态码")
    private Integer code;

    @ApiModelProperty("请求返回信息")
    private String  msg;

    @ApiModelProperty("请求返回数据")
    private T data;

    @ApiModelProperty("请求附加信息")
    private Map map=new HashMap();

    /**
     * 成功响应方法
     * @param object
     * @param <T>
     * @return
     */
    public static <T> ResModel<T> success(Code code, T object){
        ResModel<T> rm = new ResModel<>();
        rm.code=code.getCode();
        rm.msg=code.getState();
        rm.data=object;
        return rm;
    }

    /**
     * 错误响应方法
     * @param code
     * @param <T>
     * @return
     */
    public static <T> ResModel<T> error(Code code){
        ResModel<T> rm = new ResModel<>();
        rm.code=code.getCode();
        rm.msg=code.getState();
        return rm;
    }

    /**
     * 添加附加信息
     * @param key
     * @param value
     * @return
     */
    public ResModel<T> add(String key, Object value){
        this.map.put(key,value);
        return this;
    }

}
