package com.farmer.common.dto;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author 林邵晨
 * @since 2023-03-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="TbOrder对象", description="订单表")
public class OrderDTO extends Model<OrderDTO> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "订单号")
    private String orderId;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "商品")
    private List<ShopDTO> shop;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "电话")
    private String phone;

    @ApiModelProperty(value = "电话")
    private String username;

    @ApiModelProperty(value = "总价")
    private String total;

    @ApiModelProperty(value = "更新时间")
    private String createTime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}

